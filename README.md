# Chatbot Documentation

Web application that allow SMB to gather customer feedback for their Facebook Page using chat bot

### 1. Prerequisites:
To run fully functions provided by this repository, there are some requirements need to be met.
* Docker and docker-compose to run the production deployment.

### 2. Architecture
* The backend was built using Django, Django Rest Framework and PostgreSQL
* The frontend was built using React, Redux and Hook.

### 3. How to run
* Make sure there is no service is running on port 5432 (for database) and 8000 (for webapp).
* Run docker-compose command to demo production deployment
  ```bash
  docker-compose up
  ```
* Webapp is ready at: **http://127.0.0.1**
* To stop server, close **docker-compose up** terminal, then open new terminal, type below command. This command will terminate db and 
webapps containers.
  ```bash
  docker-compose down
  ```