import { combineReducers } from 'redux';
import {authSlice} from "./reducers/authSlice";

export const rootReducer = combineReducers({
    auth: authSlice.reducer,
});
