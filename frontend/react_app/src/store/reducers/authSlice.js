import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    userId: null,
    token: null,
    errorMessage: null,
}

export const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        setAuthData: (state, action) => {
            const {userId, token} = action.payload
            state.errorMessage = null
            state.userId = userId
            state.token = token
        },

        logout: (state) => {
            state.errorMessage = null
            state.userId = null
            state.token = null
        },

        setErrorMessage: (state, action) => {
            state.errorMessage = action.payload.errorMessage
        }
    },
})

export default authSlice.reducer