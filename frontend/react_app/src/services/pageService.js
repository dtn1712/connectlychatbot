import axios from "axios";

const API_URL = `${process.env.REACT_APP_API_URL}/api/${process.env.REACT_APP_API_VERSION}/pages`;

export const listPages = (headers) => {
  return axios.get(API_URL, { headers: headers })
      .then(res => {
        return Promise.resolve(res.data)
      })
      .catch(err => {
          return Promise.reject((err.response && err.response.data && err.response.data.errors) ? err.response.data.errors[0].message : err.toString());
      })
}

export const createPage = (platform, external_page_id, page_access_token, headers) => {
  return axios.post(API_URL, {
      platform,
      external_page_id,
      page_access_token
    }, { headers: headers })
      .then(res => {
        return Promise.resolve(res.data)
      })
      .catch(err => {
          return Promise.reject((err.response && err.response.data && err.response.data.errors) ? err.response.data.errors[0].message : err.toString());
      })
};

export const deletePage = (pageId, headers) => {
    return axios.delete(`${API_URL}/${pageId}`, { headers: headers })
}

export const listPageReviews = (pageId, headers) => {
  return axios.get(`${API_URL}/${pageId}/reviews`, { headers: headers })
      .then(res => {
        return Promise.resolve(res.data)
      })
      .catch(err => {
          return Promise.reject((err.response && err.response.data && err.response.data.errors) ? err.response.data.errors[0].message : err.toString());
      })
}

export const listReviewTemplates = (pageId, headers) => {
  return axios.get(`${API_URL}/${pageId}/review-templates`, { headers: headers })
      .then(res => {
        return Promise.resolve(res.data)
      })
      .catch(err => {
          return Promise.reject((err.response && err.response.data && err.response.data.errors) ? err.response.data.errors[0].message : err.toString());
      })
}

export const createReviewTemplate = (pageId, condition_type, condition_value, template_settings, headers) => {
  return axios.post(`${API_URL}/${pageId}/review-templates`, {
      condition_type,
      condition_value,
      template_settings
  }, { headers: headers })
      .then(res => {
        return Promise.resolve(res.data)
      })
      .catch(err => {
          return Promise.reject((err.response && err.response.data && err.response.data.errors) ? err.response.data.errors[0].message : err.toString());
      })
};

// const getRestaurant = (restaurantId) => {
//   return axios.get(`${API_URL}/${restaurantId}`, { headers: authHeader() })
//       .then((response) => {
//         return response.data
//       });
// }
//

//
// const updateRestaurant = (restaurantId, name, description, location, image) => {
//     return axios.patch(`${API_URL}/${restaurantId}`, {
//         name,
//         description,
//         location,
//         image
//     }, { headers: authHeader() }).then((response) => {
//         return response.data;
//     });
// };
