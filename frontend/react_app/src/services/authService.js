import axios from "axios";

const API_URL = `${process.env.REACT_APP_API_URL}/api/${process.env.REACT_APP_API_VERSION}/account`;

export const register =  (email, password) => {
    return axios.post(`${API_URL}/register`, {
          email,
          password,
        }).then(res => {
            return Promise.resolve({
                userId: res.data['user_id'],
                token: res.data['token']
            })
        }).catch(err => {
            return Promise.reject((err.response && err.response.data && err.response.data.errors) ? err.response.data.errors[0].message : err.toString());
        })
};

export const login = (email, password) => {
    return axios.post(`${API_URL}/login`, {
          email,
          password,
         }).then(res => {
            return Promise.resolve({
                userId: res.data['user_id'],
                token: res.data['token']
            })
        }).catch(err => {
            return Promise.reject((err.response && err.response.data && err.response.data.errors) ? err.response.data.errors[0].message : err.toString());
        })
};