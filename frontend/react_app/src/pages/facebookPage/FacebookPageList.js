import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Link, useNavigate} from 'react-router-dom';

import {Button, Card, Col, Container, FormControl, Row} from "react-bootstrap";
import {ViewCardBody, ViewCardHeader, ViewContainer} from "../../components/common/DefaultStyledView";
import {createPage, listPages, deletePage} from "../../services/pageService";


const FacebookPageList = (props) => {

    const navigate = useNavigate();
    const [pages, setPages] = useState([])

    const [externalPageId, setExternalPageId] = useState('')
    const [pageAccessToken, setPageAccessToken] = useState('')

    const authState = useSelector((state) => state.auth);

    const handleListPages = async () => {
        await listPages({ Authorization: "Token " + authState.token })
                .then(data => {
                    console.log(data)
                    setPages(data)
                })
                .catch(err => {
                    console.log(err)
                })
    }

    const onLinkFacebookPage = async () => {
        await createPage("facebook", externalPageId, pageAccessToken,{ Authorization: "Token " + authState.token })
            .then(data => {
                handleListPages()
            })
            .catch(err => {
                console.log(err)
            })
    }

    useEffect(() => {
        if (!authState.token) {
            navigate("/login")
        } else {
            handleListPages()
        }
    }, [])

    const handleDeletePage = async (pageId) => {
        await deletePage(pageId,{ Authorization: "Token " + authState.token })
            .then(data => {
                handleListPages()
            })
            .catch(err => {
                console.log(err)
            })
    }

    return (
        <Container>
            <ViewContainer>
                <Card style={{ marginTop: "40px"}}>
                    <ViewCardHeader>Link your Facebook page</ViewCardHeader>
                    <ViewCardBody>
                        <Row>
                          <Col xs={4} className="pt-2">
                            <Card.Title as="h6">Page Id</Card.Title>
                          </Col>
                          <Col xs={8}>
                            <FormControl type="text" name="pageId" value={externalPageId} onChange={(e) => setExternalPageId(e.target.value)}/>
                          </Col>
                        </Row>
                        <Row className="mt-20">
                          <Col xs={4} className="pt-2">
                            <Card.Title as="h6" >Page Access Token</Card.Title>
                          </Col>
                          <Col xs={8}>
                            <FormControl type="text" name="pageAccessToken"  value={pageAccessToken}  onChange={(e) => setPageAccessToken(e.target.value)} />
                          </Col>
                        </Row>
                        <Button className="float-right mb-3" variant="outline-primary" onClick={onLinkFacebookPage}>
                            Link
                        </Button>
                    </ViewCardBody>
                </Card>
            </ViewContainer>

            <Container>
                {pages && pages.length > 0 && pages.map((page, index) => (
                    <Row>
                        <Card style={{ width: "100%", marginTop: "20px", float: "left" }} key={index}>
                            <Link to={`/facebook-page/${page.id}`} style={{ textDecoration: 'none', color: "#333", padding: 0, margin:0 }}>
                                <Card.Body>
                                    <Row>
                                        <Col md={3}>
                                            <b>Page Id</b>
                                        </Col>
                                        <Col md={7}>
                                            {page.external_page_id}
                                        </Col>
                                        <Col md={2}>
                                            <Button onClick={() => handleDeletePage(page.id)}>Delete</Button>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={3}>
                                            <b>Platform</b>
                                        </Col>
                                        <Col md={9}>
                                            {page.platform}
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={3}>
                                            <b>Total Reviews</b>
                                        </Col>
                                        <Col md={9}>
                                            {page.total_reviews}
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={3}>
                                            <b>Average Score</b>
                                        </Col>
                                        <Col md={9}>
                                            {page.avg_score ? page.avg_score : '-'}
                                        </Col>
                                    </Row>
                                </Card.Body>
                            </Link>
                        </Card>
                    </Row>

                ))}

                {(!pages || pages.length === 0) && (
                    <h4 style={{ textAlign: 'center', marginTop: "50px"}}>You don't have any page</h4>
                )}
            </Container>
        </Container>
    );
};

export default FacebookPageList;
