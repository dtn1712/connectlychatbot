import React, {useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Row, Col, Container, Tab, Nav} from "react-bootstrap";

import { useParams } from "react-router-dom";

import "react-datepicker/dist/react-datepicker.css";
import ComponentLoading from "../../components/common/ComponentLoading";
import FacebookPageSettings from "../../components/facebookPage/FacebookPageSettings";
import FacebookPageReviewList from "../../components/facebookPage/FacebookPageReviewList";


const FacebookPageDetail = (props) => {


    const { pageId } = useParams()

    const dispatch = useDispatch()

    const authState = useSelector((state) => state.auth);

    const [errorMessage, setErrorMessage] = useState(null);

    const [loading, setLoading] = useState(false);


    return (
        <Container style={{marginTop: "40px"}}>
            {loading && <ComponentLoading />}

            <Tab.Container id="left-tabs-example" defaultActiveKey="first">
              <Row>
                <Col sm={3}>
                  <Nav variant="pills" className="flex-column">
                    <Nav.Item>
                      <Nav.Link eventKey="first">Reviews</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                      <Nav.Link eventKey="second">Settings</Nav.Link>
                    </Nav.Item>
                  </Nav>
                </Col>
                <Col sm={9}>
                  <Tab.Content>
                    <Tab.Pane eventKey="first">
                      <FacebookPageReviewList />
                    </Tab.Pane>
                    <Tab.Pane eventKey="second">
                      <FacebookPageSettings />
                    </Tab.Pane>
                  </Tab.Content>
                </Col>
              </Row>
            </Tab.Container>

        </Container>

    );
};

export default FacebookPageDetail;
