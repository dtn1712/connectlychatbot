import {Card, Form, Button} from "react-bootstrap"

import {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {authSlice} from "../../store/reducers/authSlice";
import {useNavigate} from "react-router-dom";
import {ViewCardBody, ViewCardHeader, ViewContainer} from "../../components/common/DefaultStyledView";
import ComponentLoading from "../../components/common/ComponentLoading";
import ErrorAlert from "../../components/common/ErrorAlert";
import {login, register} from "../../services/authService";

function Login() {

    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")
    const [loading, setLoading] = useState(false)

    const navigate = useNavigate()

    const authState = useSelector((state) => state.auth)

    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(authSlice.actions.setErrorMessage({errorMessage:null}))
    }, [dispatch])

    useEffect(() => {
        if (authState.token) {
            navigate("/")
        }
    }, [authState])

    const onLoginSubmit = (e) => {
        e.preventDefault()
        if (email.length === 0 || password.length === 0) return;

        setLoading(true)
        login(email, password)
            .then(data => dispatch(authSlice.actions.setAuthData(data)))
            .catch(errorMessage => dispatch(authSlice.actions.setErrorMessage({ errorMessage })))
            .finally(() => setLoading(false))
    }

    const onSignupSubmit = (e) => {
        e.preventDefault()
        if (email.length === 0 || password.length === 0) return;

        setLoading(true)
        register(email, password)
            .then(data => dispatch(authSlice.actions.setAuthData(data)))
            .catch(errorMessage => dispatch(authSlice.actions.setErrorMessage({ errorMessage })))
            .finally(() => setLoading(false))
    }

    return (
        <ViewContainer>
            {loading && <ComponentLoading />}
            <Card>
                <ViewCardHeader>Create account</ViewCardHeader>
                <ViewCardBody>
                    {authState.errorMessage && <ErrorAlert message={authState.errorMessage}/> }

                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)}/>
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)}/>
                    </Form.Group>

                    <div className="d-grid gap-2">
                        <Button variant="outline-primary" style={{ width: "100%"}} onClick={(e) => onSignupSubmit(e)}>
                            Signup
                        </Button>
                        <Button variant="outline-success" style={{ width: "100%", marginTop: "10px"}} onClick={(e) => onLoginSubmit(e)}>
                            Login
                        </Button>
                    </div>
                </ViewCardBody>
            </Card>
        </ViewContainer>
    )
}

export default Login