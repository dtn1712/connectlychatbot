import React from "react";
import {Route, Routes, BrowserRouter, Outlet} from "react-router-dom";

import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import FacebookPageList from "./pages/facebookPage/FacebookPageList";
import Login from "./pages/account/Login";
import Header from "./components/Header";
import FacebookPageDetail from "./pages/facebookPage/FacebookPageDetail";


function Layout() {
    return (
        <div>
          <Header />
          <div className="container">
            <Outlet />
          </div>
        </div>
    )
}

const App = () => {
  return (
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Layout />}>
            <Route index element={<FacebookPageList />} />
            <Route path="facebook-page/:pageId" element={<FacebookPageDetail />} />
            <Route path="login" element={<Login />} />
          </Route>
        </Routes>
      </BrowserRouter>
  );
};

export default App;
