import React from "react";
import { useDispatch, useSelector } from "react-redux";

import { Navbar, Nav } from 'react-bootstrap';

import {authSlice} from "../store/reducers/authSlice";

const Header = () => {

  const authState = useSelector((state) => state.auth);

  const dispatch = useDispatch();

  const handleLogout = () => {
    dispatch(authSlice.actions.logout())
  };

  return (
    <div id="header-section">
      <Navbar bg="dark" variant="dark" expand="lg">
        <Navbar.Brand href="/">Connectly Chatbot</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          {authState.token && (
            <Nav className="ml-auto">
              <Nav.Link href="/">Pages</Nav.Link>
              <Nav.Link href="/" onClick={handleLogout}>Logout</Nav.Link>
            </Nav>
          )}
          {!authState.token && (
            <Nav className="ml-auto">
              <Nav.Link href="/login">Login</Nav.Link>
            </Nav>
          )}
        </Navbar.Collapse>
      </Navbar>

    </div>
  );
};

export default Header;
