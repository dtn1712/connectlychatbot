import React, {useEffect, useState} from "react";
import {useSelector} from "react-redux";
import {useNavigate, useParams} from 'react-router-dom';

import {Button, Card, Col, Container, FormControl, Row} from "react-bootstrap";
import {ViewCardBody, ViewCardHeader} from "../../components/common/DefaultStyledView";
import {listReviewTemplates, deletePage, createReviewTemplate} from "../../services/pageService";


const FacebookPageSettings = (props) => {

    const {pageId} = useParams()

    const navigate = useNavigate();
    const [reviewTemplates, setReviewTemplates] = useState([])

    const [conditionType, setConditionType] = useState('')
    const [conditionValue, setConditionValue] = useState('')
    const [template, setTemplate] = useState('')

    const authState = useSelector((state) => state.auth);

    const handleListReviewTemplates = async () => {
        await listReviewTemplates(pageId, { Authorization: "Token " + authState.token })
                .then(data => {
                    setReviewTemplates(data)
                })
                .catch(err => {
                    console.log(err)
                })
    }

    const onCreateReviewTemplates = async () => {
        await createReviewTemplate(pageId, conditionType, conditionValue, template, { Authorization: "Token " + authState.token })
            .then(data => {
                handleListReviewTemplates()
            })
            .catch(err => {
                console.log(err)
            })
    }

    useEffect(() => {
        if (!authState.token) {
            navigate("/login")
        } else {
            handleListReviewTemplates()
        }
    }, [])

    const handleDeleteReviewTemplate = async (pageId) => {
        await deletePage(pageId,{ Authorization: "Token " + authState.token })
            .then(data => {
                handleListReviewTemplates()
            })
            .catch(err => {
                console.log(err)
            })
    }

    return (
        <>
            <Container>
                <Card style={{ marginTop: "40px"}}>
                    <ViewCardHeader>Create customer review template</ViewCardHeader>
                    <ViewCardBody>
                        <Row>
                          <Col xs={4} className="pt-2">
                            <Card.Title as="h6">Condition Trigger Type</Card.Title>
                          </Col>
                          <Col xs={8}>
                            <FormControl type="text" value={conditionType} onChange={(e) => setConditionType(e.target.value)}/>
                          </Col>
                        </Row>
                        <Row className="mt-20">
                          <Col xs={4} className="pt-2">
                            <Card.Title as="h6" >Condition Value</Card.Title>
                          </Col>
                          <Col xs={8}>
                            <FormControl type="text" value={conditionValue}  onChange={(e) => setConditionValue(e.target.value)} />
                          </Col>
                        </Row>
                        <Row className="mt-20">
                          <Col xs={4} className="pt-2">
                            <Card.Title as="h6" >Template</Card.Title>
                          </Col>
                          <Col xs={8}>
                            <FormControl as="textarea" rows={8} value={template}  onChange={(e) => setTemplate(e.target.value)} />
                          </Col>
                        </Row>
                        <Button className="float-right mb-3" variant="outline-primary" onClick={() => onCreateReviewTemplates()}>
                            Create
                        </Button>
                    </ViewCardBody>
                </Card>
            </Container>

            <Container>
                {reviewTemplates && reviewTemplates.length > 0 && reviewTemplates.map((reviewTemplate, index) => (
                    <Row>
                        <Card style={{ width: "100%", marginTop: "20px", float: "left" }} key={index}>
                                <Card.Body>
                                    <Row>
                                        <Col md={3}>
                                            <b>Template Id</b>
                                        </Col>
                                        <Col md={7}>
                                            {reviewTemplate.id}
                                        </Col>
                                        <Col md={2}>
                                            <Button onClick={() => handleDeleteReviewTemplate(reviewTemplate.id)}>Delete</Button>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={3}>
                                            <b>Condition type</b>
                                        </Col>
                                        <Col md={9}>
                                            {reviewTemplate.condition_type}
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={3}>
                                            <b>Condition value</b>
                                        </Col>
                                        <Col md={9}>
                                            {reviewTemplate.condition_value}
                                        </Col>
                                    </Row>
                                </Card.Body>
                        </Card>
                    </Row>

                ))}

                {(!reviewTemplates || reviewTemplates.length === 0) && (
                    <h4 style={{ textAlign: 'center', marginTop: "50px"}}>Your page have no template</h4>
                )}
            </Container>
        </>
    );
};

export default FacebookPageSettings;
