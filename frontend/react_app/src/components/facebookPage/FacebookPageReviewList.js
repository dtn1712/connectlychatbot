import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {Link, useNavigate, useParams} from 'react-router-dom';

import {Button, Card, Col, Container, FormControl, Row} from "react-bootstrap";
import {listPageReviews} from "../../services/pageService";


const FacebookPageReviewList = (props) => {

    const { pageId } = useParams()
    const navigate = useNavigate();
    const [pageReviews, setPageReviews] = useState([])

    const authState = useSelector((state) => state.auth);

    const handleListPageReviews = async () => {
        await listPageReviews(pageId, { Authorization: "Token " + authState.token })
                .then(data => {
                    setPageReviews(data)
                })
                .catch(err => {
                    console.log(err)
                })
    }


    useEffect(() => {
        if (!authState.token) {
            navigate("/login")
        } else {
            handleListPageReviews()
        }
    }, [])

    return (
        <Container>
            {pageReviews && pageReviews.length > 0 && pageReviews.map((pageReview, index) => (
                <Row>
                    <Card style={{ width: "100%", marginTop: "20px", float: "left" }} key={index}>
                        <Card.Body>
                            <Row>
                                <Col md={3}>
                                    <b>Score</ b>
                                </Col>
                                <Col md={9}>
                                    {pageReview.score}
                                </Col>
                            </Row>
                            <Row>
                                <Col md={3}>
                                    <b>Feedback</ b>
                                </Col>
                                <Col md={9}>
                                    {pageReview.feedback}
                                </Col>
                            </Row>
                            <Row>
                                <Col md={3}>
                                    <b>Tag</ b>
                                </Col>
                                <Col md={9}>
                                    {pageReview.tag}
                                </Col>
                            </Row>
                            <Row>
                                <Col md={3}>
                                    <b>Created at</ b>
                                </Col>
                                <Col md={9}>
                                    {pageReview.created_at}
                                </Col>
                            </Row>
                        </Card.Body>
                    </Card>
                </Row>

            ))}
            {(!pageReviews || pageReviews.length === 0) && (
                <h4 style={{ textAlign: 'center', marginTop: "50px"}}>Your page have no reviews</h4>
            )}
        </Container>
    );
};

export default FacebookPageReviewList;
