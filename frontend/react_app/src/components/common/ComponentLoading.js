
import {Oval} from "react-loader-spinner";
import styled from "styled-components";

const LoadingContainer = styled.div`
  display:flex;
  position: absolute;
  width: 100%;
  height: 100%;
  align-items: center;
  justify-content: center;
  z-index: 9999;
`;

export default function ComponentLoading() {
    return <LoadingContainer>
        <Oval
            height={40}
            width={40}
            color="#4fa94d"
            wrapperStyle={{}}
            wrapperClass=""
            visible={true}
            ariaLabel='oval-loading'
            secondaryColor="#4fa94d"
            strokeWidth={2}
            strokeWidthSecondary={2}

        />
    </LoadingContainer>
}