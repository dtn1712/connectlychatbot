import {FidgetSpinner} from "react-loader-spinner";
import styled from "styled-components";


const Container = styled.div`
  position: fixed;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  z-index: 9999;
`;

export default function PageLoading() {
    return (
        <Container>
            <FidgetSpinner
                visible={true}
                height="80"
                width="80"
                ariaLabel="loading"
                wrapperStyle={{}}
                wrapperClass="loading-wrapper"
                ballColors={['#ff0000', '#00ff00', '#0000ff']}
                backgroundColor="#F4442E"
            />
        </Container>
    )

}