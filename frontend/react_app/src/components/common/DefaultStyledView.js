import styled from "styled-components";
import {Card, Container} from "react-bootstrap";

export const ViewContainer = styled(Container)`
  margin-top: 30px;
  max-width: 540px;
  min-height: 100px;
  position: relative;
  font-size: 14px;
  color: black;

  a {
    color: var(--bs-link-color);
    text-decoration: underline;
  }
`;

export const ViewCardHeader = styled(Card.Header)`
  padding:15px 20px 15px 20px;
  font-size:16px;
  font-weight: bold;
`;

export const ViewCardBody = styled(Card.Body)`
  padding:20px;
  
  .row {
    margin-top:10px;
    margin-bottom: 10px;
  }
`;