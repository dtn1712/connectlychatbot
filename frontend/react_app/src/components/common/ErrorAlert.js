import styled from "styled-components";
import {Alert} from "react-bootstrap";

const ErrorField = styled(Alert)`
  color: red;
  font-size: 16px;
  text-align: center;
  margin-top:10px;
  margin-bottom: 10px;
`;

export default function ErrorAlert(props) {
    const {message} = props

    return (
        <ErrorField variant="danger">
            {message}
        </ErrorField>
    )
}