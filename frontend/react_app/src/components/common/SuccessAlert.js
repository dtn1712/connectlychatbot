import styled from "styled-components";
import {Alert} from "react-bootstrap";

const SuccessField = styled(Alert)`
  color: green;
  font-size: 16px;
  text-align: center;
  margin-top:10px;
  margin-bottom: 10px;
`;

export default function SuccessAlert(props) {
    const {message} = props

    return (
        <SuccessField variant="success">
            {message}
        </SuccessField>
    )
}