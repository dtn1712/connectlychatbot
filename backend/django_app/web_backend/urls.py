"""web_backend URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import include, path
from rest_framework.urlpatterns import format_suffix_patterns

from web_backend import settings
from web_backend.rest_api.controllers import account, page
from web_backend.rest_api.webhook import messenger

urlpatterns = [
    path('account/login', account.LoginController.as_view()),
    path('account/register', account.RegisterController.as_view()),

    path('pages', page.PageListController.as_view()),
    path('pages/<int:page_id>', page.PageDetailController.as_view()),
    path('pages/<int:page_id>/review-templates', page.PageReviewTemplateListController.as_view()),
    path('pages/<int:page_id>/review-templates/<int:template_id>', page.PageReviewTemplateDetailController.as_view()),
    path('pages/<int:page_id>/reviews', page.PageReviewListController.as_view()),
]

if settings.API_URL_PREFIX:
    urlpatterns = [path(f'{settings.API_URL_PREFIX}/', include(urlpatterns))]

urlpatterns = format_suffix_patterns(urlpatterns)

webhook_urlpatterns = [
    path('webhook/messenger/', messenger.MessagesWebhook.as_view()),
]

urlpatterns += webhook_urlpatterns
