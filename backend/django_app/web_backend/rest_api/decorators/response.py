import json
import logging

from django.core.exceptions import ObjectDoesNotExist
from rest_framework.exceptions import ValidationError, AuthenticationFailed, NotAuthenticated, PermissionDenied, \
    ErrorDetail
from rest_framework import status
from rest_framework.response import Response

logger = logging.getLogger(__name__)


def get_error_message(exception):
    if isinstance(exception, list):
        errors = []
        for item in exception:
            errors.append(get_error_message(item))
        return errors
    elif isinstance(exception, ErrorDetail):
        return {
            "message": exception.__str__()
        }
    elif isinstance(exception, dict):
        errors = []
        for key, value in exception.items():
            errors.append({
                "type": key,
                "message": value[0].__str__()
            })
        return errors
    else:
        return {
            "message": str(exception)
        }


def get_exception_response(exception, http_status):
    return Response({
        "errors": get_error_message(exception)
    }, status=http_status)


def render_response(original_function):
    def new_function(*args, **kwargs):
        try:
            return original_function(*args, **kwargs)
        except ValidationError as e:
            logger.exception(e)
            return get_exception_response(e.detail, status.HTTP_400_BAD_REQUEST)
        except (AuthenticationFailed, NotAuthenticated) as e:
            logger.exception(e)
            return get_exception_response(e.detail, status.HTTP_401_UNAUTHORIZED)
        except PermissionDenied as e:
            logger.exception(e)
            return get_exception_response(e.detail, status.HTTP_403_FORBIDDEN)
        except ObjectDoesNotExist as e:
            logger.exception(e)
            return get_exception_response(e, status.HTTP_404_NOT_FOUND)
        except Exception as e:
            logger.exception(e)
            return get_exception_response(e, status.HTTP_500_INTERNAL_SERVER_ERROR)

    return new_function
