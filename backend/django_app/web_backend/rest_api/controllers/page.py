from rest_framework import status
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from web_backend.rest_api.decorators.response import render_response
from web_backend.rest_api.services.page import PageService, PageReviewService, PageReviewTemplateService


class PageListController(APIView):

    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.page_service = PageService.get_instance()

    @render_response
    def get(self, request):
        user = request.user
        pages = self.page_service.find_pages_by_user(user)
        print (pages)
        return Response(pages)

    @render_response
    def post(self, request):
        user = request.user
        data = dict(request.data)
        page = self.page_service.create_page(user, data)
        return Response(page, status=status.HTTP_201_CREATED)


class PageDetailController(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.page_service = PageService.get_instance()

    @render_response
    def get(self, request, page_id):
        page = self.page_service.get_page(page_id)
        return Response(page)

    @render_response
    def patch(self, request, page_id):
        user = request.user
        data = dict(request.data)
        update_page = self.page_service.update_page(user, page_id, data)
        return Response(update_page)

    @render_response
    def delete(self, request, page_id):
        user = request.user
        self.page_service.delete_page(user, page_id)
        return Response(status=status.HTTP_204_NO_CONTENT)


class PageReviewListController(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.page_review_service = PageReviewService.get_instance()

    @render_response
    def get(self, request, page_id):
        reviews = self.page_review_service.get_page_reviews(page_id)
        return Response(reviews)


class PageReviewTemplateListController(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.page_review_template_service = PageReviewTemplateService.get_instance()

    @render_response
    def get(self, request, page_id):
        review_templates = self.page_review_template_service.find_review_templates_by_page(page_id)
        return Response(review_templates)

    @render_response
    def post(self, request, page_id):
        user = request.user
        data = dict(request.data)
        review = self.page_review_template_service.create_page_review_template(user, page_id, data)
        return Response(review, status=status.HTTP_201_CREATED)


class PageReviewTemplateDetailController(APIView):
    authentication_classes = [TokenAuthentication]
    permission_classes = [IsAuthenticated]

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.page_review_service = PageReviewService.get_instance()

    @render_response
    def get(self, request, page_id, review_id):
        review = self.page_review_service.get_page_review(page_id, review_id)
        return Response(review)

    @render_response
    def patch(self, request, page_id, review_id):
        user = request.user
        data = dict(request.data)
        update_review = self.page_review_service.update_page_review(user, page_id, review_id, data)
        return Response(update_review)

    @render_response
    def delete(self, request, page_id, review_id):
        user = request.user
        self.page_review_service.delete_page_review(user, page_id, review_id)
        return Response()

