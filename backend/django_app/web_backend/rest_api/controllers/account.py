from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from web_backend.rest_api.decorators.response import render_response
from web_backend.rest_api.services.account import AccountService


class RegisterController(APIView):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.account_service = AccountService.get_instance()

    @render_response
    def post(self, request):
        data = dict(request.data)
        register_response = self.account_service.register(data)
        return Response(register_response, status=status.HTTP_201_CREATED)


class LoginController(APIView):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.account_service = AccountService.get_instance()

    @render_response
    def post(self, request):
        data = dict(request.data)
        login_response = self.account_service.login(data)
        return Response(login_response)
