from django.apps import AppConfig
from django.db.models.signals import post_migrate


class RestApiConfig(AppConfig):
    name = 'web_backend.rest_api'
    verbose_name = 'Rest API'

    def ready(self):
        import web_backend.rest_api.signals
