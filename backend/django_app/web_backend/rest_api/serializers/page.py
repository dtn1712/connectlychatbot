from django.db.models import Avg
from rest_framework import serializers

from web_backend.rest_api.models import Page, PageReview, PageReviewTemplate


class PageSerializer(serializers.ModelSerializer):
    external_page_id = serializers.CharField()
    page_access_token = serializers.CharField()
    avg_score = serializers.SerializerMethodField()
    total_reviews = serializers.SerializerMethodField()

    class Meta:
        model = Page
        fields = '__all__'

    def get_avg_score(self, obj):
        values = PageReview.objects.filter(page__pk=obj.pk).aggregate(avg_score=Avg("score")).values()
        return list(values)[0]

    def get_total_reviews(self, obj):
        return PageReview.objects.filter(page__pk=obj.pk).count()


class PageCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Page
        fields = ['external_page_id', 'page_access_token']

    def create(self, validated_data):
        return Page(**validated_data)


class PageUpdateSerializer(PageCreateSerializer):
    external_page_id = serializers.CharField(required=True)
    page_access_token = serializers.CharField(required=True)

    class Meta:
        model = Page
        fields = ['external_page_id', 'page_access_token']

    def update(self, instance, validated_data):
        external_page_id = validated_data.get("external_page_id")
        page_access_token = validated_data.get("page_access_token")

        if external_page_id is not None and len(external_page_id) > 0:
            instance.external_page_id = external_page_id

        if page_access_token is not None and len(page_access_token) > 0:
            instance.page_access_token = page_access_token

        return instance


class PageDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Page
        fields = '__all__'

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        representation['avg_score'] = self.context['avg_score']
        representation['highest_score'] = self.context['highest_score']
        representation['lowest_score'] = self.context['lowest_score']
        representation['reviews'] = self.context['reviews']

        return representation


class PageReviewSerializer(serializers.ModelSerializer):
    class Meta:
        model = PageReview
        fields = "__all__"


class PageReviewCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = PageReview
        exclude = ['page']


class PageReviewTemplateSerializer(serializers.ModelSerializer):
    class Meta:
        model = PageReviewTemplate
        fields = "__all__"


class PageReviewTemplateCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = PageReviewTemplate
        exclude = ['page']
