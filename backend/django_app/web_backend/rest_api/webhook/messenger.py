from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from web_backend import settings
from web_backend.rest_api.services.chatbot import ChatBotServiceFactory
from web_backend.rest_api.services.page import PageReviewService, PageService
import logging

logger = logging.getLogger(__name__)


class MessagesWebhook(APIView):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.chatbot_service = ChatBotServiceFactory.get_service(settings.FACEBOOK_MESSENGER_PLATFORM)
        self.page_review_service = PageReviewService.get_instance()
        self.page_service = PageService.get_instance()

    @staticmethod
    def _construct_review_data(messaging_feedback):
        reviews = []
        try:
            questions = messaging_feedback['feedback_screens'][0]['questions']
            for key, value in questions.items():
                reviews.append({
                    "tag": key,
                    "score": questions[key]['payload'],
                    "feedback": questions[key]['follow_up']['payload']
                })
        except Exception as e:
            logger.error("Failed to parse review data", e)

        return reviews

    def get(self, request):
        request_params = request.query_params
        if request_params.get("hub.mode") == "subscribe" and request_params.get("hub.challenge"):
            if not request_params.get("hub.verify_token") == settings.MESSENGER_VERIFY_TOKEN:
                return Response("Verification token missmatch", status=status.HTTP_403_FORBIDDEN)

            return Response(int(request_params.get("hub.challenge")), status=status.HTTP_200_OK)
        return Response()

    def post(self, request):
        data = dict(request.data)
        entry = data['entry'][0]
        if 'messaging' in entry and len(entry['messaging']) > 0:
            messaging_entry = entry['messaging'][0]
            external_page_id = entry['id']
            page = self.page_service.find_by_platform_and_external_page_id(settings.FACEBOOK_MESSENGER_PLATFORM,
                                                                           external_page_id)

            if 'messaging_feedback' in messaging_entry:
                reviews = self._construct_review_data(messaging_entry['messaging_feedback'])
                for review in reviews:
                    self.page_review_service.create_page_review(page.pk, review)
            else:
                self.chatbot_service.process_message(page, settings.MESSAGE_CONDITION_TYPE, messaging_entry)
        return Response()
