import json
import logging
from abc import abstractmethod
import requests

from web_backend import settings
from web_backend.rest_api.services.page import PageReviewTemplateService, PageService

logger = logging.getLogger(__name__)


class ChatBotService:

    @abstractmethod
    def process_message(self, page, condition_type, message_data):
        pass


class FacebookChatBotService(ChatBotService):

    _instance = None

    endpoint_url = "https://graph.facebook.com/v7.0/me/messages?access_token={}"
    headers = {
        "Content-type": "application/json"
    }

    def __init__(self):
        self.review_template_service = PageReviewTemplateService.get_instance()
        self.page_service = PageService.get_instance()

    def _construct_api_endpoint(self, page_access_token):
        return self.endpoint_url.format(page_access_token)

    @staticmethod
    def _construct_payload(sender_id, review_template):
        data = {
            "recipient": {"id": sender_id},
            "message": json.loads(review_template.template_settings)
        }
        return json.dumps(data)

    @staticmethod
    def _get_sentiment_analysis(message_data):
        try:
            sentiment = message_data['message']['nlp']['traits']['wit$sentiment'][0]
            sentiment_value = sentiment['value']
            sentiment_confidence = sentiment['confidence']
            return sentiment_value, sentiment_confidence
        except Exception as e:
            logger.error("Failed to get sentiment analysis", e)

        return None, 0

    @staticmethod
    def _get_original_text(message_data):
        try:
            original_text = message_data['message']['text']
            return original_text
        except Exception as e:
            logger.error("Failed to get original text", e)

        return None

    @staticmethod
    def _get_send_review_template(review_templates, original_text, sentiment_value, sentiment_confidence):
        if sentiment_confidence >= settings.MINIMUM_SENTIMENT_CONFIDENCE and sentiment_value is not None:
            for review_template in review_templates:
                condition_values = review_template.condition_value.split(",")
                if sentiment_value in condition_values:
                    return review_template
        elif original_text is not None:
            for review_template in review_templates:
                condition_values = review_template.condition_value.split(",")
                if original_text in condition_values:
                    return review_template

        return None

    @classmethod
    def get_instance(cls):
        if not cls._instance:
            cls._instance = FacebookChatBotService()
        return cls._instance

    def process_message(self, page, condition_type, message_data):
        try:
            review_templates = self.review_template_service.find_by_page_and_condition_type(page, condition_type)

            if len(review_templates) == 0:
                logger.info("Cannot find review template for page id {}".format(page.id))
            else:
                original_text = self._get_original_text(message_data)
                sentiment_value, sentiment_confidence = self._get_sentiment_analysis(message_data)
                send_review_template = self._get_send_review_template(review_templates, original_text,
                                                                      sentiment_value, sentiment_confidence)
                if send_review_template is not None:
                    url = self._construct_api_endpoint(send_review_template.page.page_access_token)
                    sender_id = message_data['sender']['id']

                    logger.info("Sending review template to {}".format(sender_id))
                    response = requests.post(url, headers=self.headers,
                                             data=self._construct_payload(sender_id, send_review_template))

                    if response.status_code == 200:
                        logger.info(
                            "Successfully sent review template {} to user {}".format(send_review_template.pk,
                                                                                     sender_id))
                    else:
                        logger.error("Failed to to send review template {} to user {}. Error is: {}".format(
                            send_review_template.pk, sender_id, response.text))
                else:
                    logger.info("No review template matched with the scenario. Skip sending review")

        except Exception as e:
            logger.error("Failed to process message", e)


default_chatbot_service = FacebookChatBotService.get_instance()


class ChatBotServiceFactory:

    @staticmethod
    def get_service(platform):
        if platform == settings.FACEBOOK_MESSENGER_PLATFORM:
            return FacebookChatBotService.get_instance()

        return default_chatbot_service
