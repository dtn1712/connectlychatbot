import logging

from django.core.exceptions import ObjectDoesNotExist, PermissionDenied
from django.db.models import Avg, Max, Min

from web_backend.rest_api.serializers.page import PageCreateSerializer, PageUpdateSerializer, PageSerializer, \
    PageDetailSerializer, PageReviewSerializer, PageReviewCreateSerializer, PageReviewTemplateSerializer, \
    PageReviewTemplateCreateSerializer
from web_backend.rest_api.models import Page, PageReview, PageReviewTemplate

logger = logging.getLogger(__name__)


class PageReviewService:
    _instance = None

    @classmethod
    def get_instance(cls):
        if not cls._instance:
            cls._instance = PageReviewService()
        return cls._instance

    @staticmethod
    def _serialize_page_review_create_data(data):
        s = PageReviewCreateSerializer(data=data)
        s.is_valid(raise_exception=True)
        return s.create(data)

    @staticmethod
    def _to_reviews(reviews):
        serializer = PageReviewSerializer(reviews, many=True)
        return serializer.data

    @staticmethod
    def _to_review(review):
        serializer = PageReviewSerializer(review)
        return serializer.data

    @staticmethod
    def get_page_review_rating_summary(page_id):
        rating_summary = PageReview.objects.filter(page__pk=page_id)\
                                                 .aggregate(avg_score=Avg("score"),
                                                            highest_score=Max("score"),
                                                            lowest_score=Min("score"))
        return {
            "avg_score": rating_summary['avg_score'],
            "highest_score": rating_summary['highest_score'],
            "lowest_score": rating_summary['lowest_score']
        }

    def get_page_reviews(self, page_id):
        reviews = PageReview.objects.filter(page__pk=page_id)\
                                          .order_by("-created_at")
        return self._to_reviews(reviews)

    def create_page_review(self, page_id, review_data):
        try:
            review_data['page'] = Page.objects.get(pk=page_id)
            create_review = self._serialize_page_review_create_data(review_data)
            create_review.save()
            return self._to_review(create_review)
        except Page.DoesNotExist:
            raise ObjectDoesNotExist("Cannot find page with id {}".format(page_id))


class PageReviewTemplateService:
    _instance = None

    @classmethod
    def get_instance(cls):
        if not cls._instance:
            cls._instance = PageReviewTemplateService()
        return cls._instance

    @staticmethod
    def _serialize_page_review_template_create_data(data):
        s = PageReviewTemplateCreateSerializer(data=data)
        s.is_valid(raise_exception=True)
        return s.create(data)

    @staticmethod
    def _to_review_templates(reviews):
        serializer = PageReviewTemplateSerializer(reviews, many=True)
        return serializer.data

    @staticmethod
    def _to_review_template(review):
        serializer = PageReviewTemplateSerializer(review)
        return serializer.data

    def find_review_templates_by_page(self, page_id):
        review_templates = PageReviewTemplate.objects.filter(page__pk=page_id).order_by("-created_at")
        return self._to_review_templates(review_templates)

    def find_by_page_and_condition_type(self, page, condition_type):
        return PageReviewTemplate.objects.filter(page__pk=page.pk, condition_type=condition_type)

    def create_page_review_template(self, created_by_user, page_id, review_data):
        try:
            page = Page.objects.get(pk=page_id)

            if created_by_user.pk != page.user.pk:
                raise PermissionDenied("You don't have permission to perform this action")

            review_data['page'] = page
            create_review_template = self._serialize_page_review_template_create_data(review_data)
            create_review_template.save()

            return self._to_review_template(create_review_template)
        except Page.DoesNotExist:
            raise ObjectDoesNotExist("Cannot find page with id {}".format(page_id))


class PageService:
    _instance = None

    def __init__(self):
        self.page_review_service = PageReviewService.get_instance()

    @classmethod
    def get_instance(cls):
        if not cls._instance:
            cls._instance = PageService()
        return cls._instance

    @staticmethod
    def _serialize_page_create_data(data):
        s = PageCreateSerializer(data=data)
        s.is_valid(raise_exception=True)
        return s.create(data)

    @staticmethod
    def _serialize_page_update_data(instance, data):
        s = PageUpdateSerializer(data=data)
        s.is_valid(raise_exception=True)
        return s.update(instance, data)

    @staticmethod
    def _to_pages(pages):
        serializer = PageSerializer(pages, many=True)
        return serializer.data

    def _to_page_detail(self, page):
        review_rating_summary = self.page_review_service.get_page_review_rating_summary(page.pk)
        context = {
            **review_rating_summary,
            "reviews": self.page_review_service.get_page_reviews(page.pk)
        }
        serializer = PageDetailSerializer(page, context=context)
        return serializer.data

    def find_pages_by_user(self, user):
        pages = Page.objects.filter(user__pk=user.pk).annotate(avg_score=Avg("page_review__score"))\
                                                     .order_by("-avg_score")
        return self._to_pages(pages)

    def find_by_platform_and_external_page_id(self, platform, external_page_id):
        pages = Page.objects.filter(platform=platform, external_page_id=external_page_id)
        if len(pages) == 0:
            raise ObjectDoesNotExist("Cannot find page with platform {} and external page id {}".format(platform,
                                                                                                        external_page_id))

        return pages[0]

    def get_page(self, page_id):
        try:
            page = Page.objects.get(pk=page_id)
            return self._to_page_detail(page)
        except Page.DoesNotExist:
            raise ObjectDoesNotExist("Cannot find page with id {}".format(page_id))

    def create_page(self, created_by_user, page_data):
        create_page = self._serialize_page_create_data(page_data)
        create_page.user = created_by_user
        create_page.save()

        return self._to_page_detail(create_page)

    def update_page(self, update_by_user, update_page_id, update_page_data):

        try:
            current_page = Page.objects.get(pk=update_page_id)
            if current_page.user.pk != update_by_user.pk:
                raise PermissionDenied("You don't have permission to perform this action")

            update_page = self._serialize_page_update_data(current_page, update_page_data)
            update_page.save()

            return self._to_page_detail(update_page)
        except Page.DoesNotExist:
            raise ObjectDoesNotExist("Cannot find page with id {}".format(update_page_id))

    def delete_page(self, delete_by_user, delete_restaurant_id):
        try:
            page = Page.objects.get(pk=delete_restaurant_id)
            if page.user.pk != delete_by_user.pk:
                raise PermissionDenied("You don't have permission to perform this action")

            page.delete()

        except Page.DoesNotExist:
            raise ObjectDoesNotExist("Cannot find page with id {}".format(delete_restaurant_id))

