import logging

from django.contrib.auth import authenticate, get_user_model
from django.db import IntegrityError
from django.utils import timezone
from rest_framework.authtoken.models import Token
from rest_framework.exceptions import ValidationError
from web_backend.rest_api.serializers.account import AccountRegistrationSerializer, AccountLoginSerializer


logger = logging.getLogger(__name__)


class AccountService:
    _instance = None

    def __init__(self):
        self.user_model = get_user_model()

    @classmethod
    def get_instance(cls):
        if not cls._instance:
            cls._instance = AccountService()
        return cls._instance

    @staticmethod
    def _validate_account_registration(data):
        s = AccountRegistrationSerializer(data=data)
        s.is_valid(raise_exception=True)

    @staticmethod
    def _validate_account_login(data):
        s = AccountLoginSerializer(data=data)
        s.is_valid(raise_exception=True)

    def register(self, registration_data):
        self._validate_account_registration(registration_data)
        try:
            user = self.user_model.objects.create_user(email=registration_data['email'],
                                                       password=registration_data['password'],
                                                       first_name=registration_data.get("first_name", ""),
                                                       last_name=registration_data.get("last_name", ""))

        except IntegrityError as e:
            logger.error(e)
            raise ValidationError("Email already exist")

        token, created = Token.objects.get_or_create(user=user)
        return {
            "user_id": user.pk,
            "token": token.key
        }

    def login(self, login_data):
        self._validate_account_login(login_data)
        user = authenticate(email=login_data['email'], password=login_data['password'])
        if not user:
            raise ValidationError('Unable to log in with provided credentials')

        user.last_login = timezone.now()
        user.save()

        token, created = Token.objects.get_or_create(user=user)
        return {
            "user_id": user.pk,
            "token": token.key
        }
