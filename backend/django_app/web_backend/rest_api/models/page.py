from django.db import models

from web_backend import settings


class Page(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="user_page")
    platform = models.CharField(max_length=50)
    external_page_id = models.CharField(max_length=100)
    page_access_token = models.CharField(max_length=300)

    # Audit field
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'page'


class PageReviewTemplate(models.Model):
    page = models.ForeignKey(Page, on_delete=models.CASCADE, related_name="page_review_template")
    template_settings = models.TextField()
    condition_type = models.CharField(choices=settings.CONDITION_TYPES, max_length=30)
    condition_value = models.CharField(max_length=100, blank=True)

    # Audit field
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'page_review_template'


class PageReview(models.Model):
    page = models.ForeignKey(Page, on_delete=models.CASCADE, related_name="page_review")
    tag = models.CharField(max_length=100, blank=True)
    score = models.IntegerField()
    feedback = models.CharField(max_length=3000)

    # Audit field
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'page_review'
