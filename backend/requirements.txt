Django==3.2
djangorestframework==3.12.2
psycopg2-binary==2.8.5
sqlparse==0.4.1
django-cors-headers
gunicorn==20.0.4
requests